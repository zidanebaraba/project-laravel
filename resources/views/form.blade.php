<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name : </label><br>
        <input type="text" name="fname"><br>
        <label>Last Name : </label><br>
        <input type="text" name="lname"><br>
        <label>Gender : </label><br>
        <input type="radio" id="Male" name="Gender" value="Male">
        <label for="Male">Male</label><br>
        <input type="radio" id="Female" name="Gender" value="Female">
        <label for="Female">Female</label><br>
        <label>Nationality : </label><br>
        <select>
            <option value="">Indonesian</option>
            <option value="">American</option>
            <option value=""></option>
        </select><br>
        <label>Language Spoken : </label><br>
        <input type="checkbox" id="bhs" name="bhs" value="Bike">
        <label for="bhs"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="eng" name="eng" value="eng">
        <label for="eng"> English</label><br>
        <input type="checkbox" id="oth" name="oth" value="oth">
        <label for="oth"> Other</label>
        <label>Bio : </label><br>
        <textarea cols="30" rows="10"></textarea><br>
        <input type="submit" Value="Sign Up">
    </form>
</body>

</html>